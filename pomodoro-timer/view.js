/// view.js

var view = {
  "initialize": function(workTime) {
    $("#stop-button").attr("disabled", true);
    this.setTimerDisplay(workTime, 0);
  },
  
  "startTimer": function(isWorkTime) {
    $("#work-time-input").attr("disabled", true);
    $("#break-time-input").attr("disabled", true);
    $("#start-button").attr("disabled", true);
    $("#stop-button").attr("disabled", false);
    this.showCoffeeCup(isWorkTime);
  },
  
  "stopTimer": function() {
    $("#work-time-input").attr("disabled", false);
    $("#break-time-input").attr("disabled", false);
    $("#start-button").attr("disabled", false);
    $("#stop-button").attr("disabled", true);
  },

  "showCoffeeCup": function(isWorkTime) {
    let indicator = isWorkTime ? '&nbsp;' : '&#x2615;';
    $('#coffee-break').html(indicator);
  },

  "getWorkTime": function() {
    return $("#work-time-input").val();
  },

  "setTimerDisplay": function(minutes, seconds) {
    $("#minutes").text(minutes);
    
    if (seconds < 10) {
      seconds = "0" + seconds;
    }
    $("#seconds").text(seconds);
    document.title = minutes + ":" + seconds;
  }
};


