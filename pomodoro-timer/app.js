/// app.js

$(document).ready(main);

function main() {
  var timer;
  var workTime = 25;
  var breakTime = 5;
  var duration = 0;
  var isWorkTime = true;
  
  var TimerStopped = function() {
    console.log("timer has reached 0:00");
    /// TODO - check which timer was running
    /// if workTime then 
    ///   next timer will be breakTime
    /// if breakTime then 
    ///   workTime
    
    if (isWorkTime) {
      isWorkTime = false;
      duration = breakTime;
    } else {
      isWorkTime = true;
      duration = workTime;
    }
    timer = CreatePomodoroTimer(duration, TimerStopped);
    timer.start();
    view.startTimer(isWorkTime);
  };
  
  view.initialize(workTime);

  $("#start-button").click(function() {
    console.log("start");
    if (timer === undefined) {
      /// when timer reaches 0:00,
      /// view.stopTimer will get called to update the UI.
      timer = CreatePomodoroTimer(workTime, TimerStopped);
    }
    timer.start();
    view.startTimer(isWorkTime);
  });

  $("#stop-button").click(function() {
    console.log("stop");
    timer.stop();
    view.stopTimer();
  });

  $("#reset").click(function() {
    console.log("reset");
    timer.stop();
    timer = undefined;
    view.stopTimer();
    view.initialize(workTime);
  });
  
  $("#timer-display").click(function() {
    if (timer && timer.isRunning()) {
      console.log("stop timer");
      $('#stop-button').click();
    } else {
      console.log("start timer");
      $('#start-button').click();
    }
  });

  $("#work-time-input").change(function(e) {
    let val = view.getWorkTime();
    workTime = val;

    view.setTimerDisplay(val, 0);
    console.log("work time changed to:", val);
  });

  $("#break-time-input").change(function(e) {
    let val = $("#break-time-input").val();
    breakTime = val;
    console.log("break time changed to:", val);
  });
}

function CreatePomodoroTimer(minutes, cbTimeStop) {
  var secondsRemaining = 0;
  var minutesRemaining = minutes;
  var intervalId = 0;

  var checkTime = function() {
    /// display time first
    view.setTimerDisplay(minutesRemaining, secondsRemaining);
    
    /// decrement time
    secondsRemaining--;
    
    if (secondsRemaining < 0) {
      secondsRemaining = 59;
      minutesRemaining--;
    }
    
    console.log("check time");
    console.log("time left: ", minutesRemaining, secondsRemaining);
    
    if (minutesRemaining < 0) {    
      stopTimer();
      cbTimeStop();
    }
  };
  
  var stopTimer = function() {
      clearInterval(intervalId);
      intervalId = 0;
      console.log("clear Interval:", intervalId);
  };

  return {
    
    "start": function() {
      console.log("start timer");
      intervalId = setInterval(checkTime, 1000);
      console.log("started timer");
    },

    "stop": function() {
      console.log("stop timer");
      stopTimer();
    },

    "isRunning": function() {
        return intervalId !== 0;
    },

    "isStopped": function() {
        return intervalId === 0;
    }
  };
}

