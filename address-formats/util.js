/// util.js

const str_html_return = '<br>';

var util = {
  toTitleCase: function(strItem) {
    /// This function was created just for title casing the country names
    let words = strItem.toLowerCase().split(' ');
    let a = words.map(function(word) {
      if (word.startsWith('d\'')) {
        return 'd\'' + word.charAt(2).toUpperCase() + word.slice(3);
      } else {
        return word.charAt(0).toUpperCase() + word.slice(1);
      }
    });
    return a.join(' ');
  },

  toInputForm: function(localeData, useLatinFormat) {

    var createMenuItem = function(obj) {
      return `<option value="${obj.value}">${obj.text}</option>`;
    };

    var HtmlFactory = function(localeData) {
      var table = {
        "name": function() {
          return '<p><input type="text" class="form-control mb-2" name="persons-name" placeholder="Name"></p>';
        },
        "org": function() {
          return '<p><input type="text" class="form-control mb-2" name="organization" placeholder="Organization"></p>';
        },
        "address": function() {
          var required = '';
          if (localeData.require.search("A") >= 0) {
           required = ' (required)&lrm;';
          }
          return `<p><input type="text" class= "form-control mb-2" name="address" placeholder="Address${required}" /></p>`;
        },

        "dep-locality": function() {
          console.log("sublocality_name_type:", localeData.sublocality_name_type);
          var text = util.toTitleCase(localeData.sublocality_name_type);
          return `<p><input type="text" class="form-control mb-2" name="dep-locality" placeholder="${text}" /></p>`;
        },

        "locality": function() {
          if (localeData.locality_name_type === undefined) {
            return '';
          }
          var placeholderText = util.toTitleCase(localeData.locality_name_type);
   
          if (localeData.require.search("C") >= 0) {
            placeholderText = `${placeholderText} (required)&lrm;`;
          } 
          return `<p><input type="text" class="form-control mb-2" name="city" placeholder="${placeholderText}" /></p>`;
        },

        "admin-area": function() {
          return `<p><input type="text" class="form-control mb-2" name="state" placeholder="${localeData.state_name_type}" /></p>`;
        },

        "postal-code": function() {

          if (localeData.zip_name_type === undefined){
            return '';
          }

          var text = `${util.toTitleCase(localeData.zip_name_type)} code`;

          if (localeData.require.search("Z") >= 0) {
            text += " (required)&lrm;";
          }
          return `<p>
            <input type="text" class="form-control mb-2" name="postal-code" placeholder="${text}" pattern="${localeData.zip}" />
          </p>`;
        },

        "sort-code": function() {
          return '<p><input type="text" class="form-control mb-2" name="sort-code" placeholder="Sort Code"></p>';
        },
      };

      return {
        "createInputTag": function(fieldType) {
          return table[fieldType]();
        }
      };
      
    };

    let strTemp;
    let str_format = useLatinFormat ? localeData['lfmt'] : localeData['fmt'];

    let str_html = str_format.replace(/[ ,/]|%n/g, "");

    var htmlFactory = HtmlFactory(localeData);

    str_html = str_html.replace("%N", htmlFactory.createInputTag("name"));
    str_html = str_html.replace("%O", htmlFactory.createInputTag("org"));

    if (localeData.sublocality_name_type) {
      str_html = str_html.replace("%D", htmlFactory.createInputTag("dep-locality"));
    }

    str_html = str_html.replace("%C", htmlFactory.createInputTag("locality"));
    str_html = str_html.replace("%A", htmlFactory.createInputTag("address"));

    /// If keys 'state_keys' and 'state_names' exist
    /// build a dropdown menu instead

    let hasStateKeys = localeData.hasOwnProperty("state_keys");
    let hasStateNames = localeData.hasOwnProperty("state_names");

    if (localeData.state_name_type) {
      strTemp = util.toTitleCase(localeData.state_name_type);
    }

    if (localeData.require.search("S") >= 0)  {
      strTemp += " (required)&lrm;";
    }

    if (hasStateKeys && hasStateKeys) {
        var arr_menu_items = [
          createMenuItem(
            { value: "", text: `select ${strTemp}` }
          )
        ];

        var state_names = useLatinFormat ? localeData.state_names_latin : localeData.state_names;

        for (let i = 0; i < localeData.state_keys.length; i += 1) {

          let str_menu_item = createMenuItem({
            value: `${localeData.state_keys[i]}`, 
            text: `${state_names[i]}`
          });

          arr_menu_items.push(str_menu_item);

          console.log(arr_menu_items);
        }

        let str_menu_items = arr_menu_items.join('\n');
        const str_html_state_menu = `<p><select class="form-control mb-2" id="select-state-names">${str_menu_items}</select></p>`;
        str_html = str_html.replace("%S", str_html_state_menu);
    } else {
      str_html = str_html.replace("%S", htmlFactory.createInputTag("admin-area"));
    }

    /// zip/postal code 

    str_html = str_html.replace("%Z", htmlFactory.createInputTag("postal-code"));

    return str_html.replace(/%X/g, htmlFactory.createInputTag("sort-code"));
  },

  toOutputForm: function(localeData, useLatinFormat) {
    var toUpper = function(fieldType, field) {
      return localeData.upper.search(fieldType) >= 0 ? field.toUpperCase() : field;
    };

    let str_temp;
    let str_format = useLatinFormat ? localeData['lfmt'] : localeData['fmt'];

    str_format = str_format.replace("%O", "[Organization]");
    str_format = str_format.replace("%N", "[Name]");
    str_format = str_format.replace(/%n/g, str_html_return);

    /// DOES SUBLOCALITY EVER NEED TO BE UPPERCASED ???
    str_temp = `[${localeData.sublocality_name_type}]`;
    str_format = str_format.replace("%D", str_temp);

    /// city 
    str_temp = toUpper("C", `[${localeData.locality_name_type}]`);
    str_format = str_format.replace("%C", str_temp);

    /// address line 
    str_temp = toUpper("A", "[Address]");
    str_format = str_format.replace("%A", str_temp);

    /// state 
    str_temp = toUpper("S", `[${localeData.state_name_type}]`);
    str_format = str_format.replace("%S", str_temp);

    str_temp = `[${localeData.zip_name_type} code]`;
    str_format = str_format.replace("%Z", str_temp);

    str_temp = toUpper("X", "[Sort Code]");
    str_format = str_format.replace(/%X/g, str_temp);

    if (useLatinFormat) {
      str_format += str_html_return + localeData.name;
    }
    return str_format;
  }
};
