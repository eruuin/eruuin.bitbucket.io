/// model.js

/*
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/all
https://stackoverflow.com/questions/38180080/when-to-use-promise-all

https://chromium-i18n.appspot.com/ssl-address
https://chromium-i18n.appspot.com/ssl-address/data
https://chromium-i18n.appspot.com/ssl-address/examples

*/

var model = {
  init: function(arr_locales, onReadyFunction) {
    const baseRequest = "https://chromium-i18n.appspot.com/ssl-address/data";
    const getCountryCodesRequest = baseRequest;
  
    var createPromise = function(countryCode) {
      return new Promise((resolve, reject) => {
        let url = `${baseRequest}/${countryCode}`;
        $.getJSON(url, function(data) {
          if (data.key === undefined) {
            data.key = "ZZ";
            data.name = "default";
          }
          console.log("%s: %s", data.name, data.fmt);
          resolve(data);
        });
      });
    };

    let arr_promises = [ ];

    arr_locales.forEach(function(element, index, array) {
      console.log("element:", element);
      /// TODO: a better test is if it's not two upper cases letters
      if (element !== "--") {
        arr_promises.push(createPromise(element));
      }
    });

    var dict_addr_formats = {};

    var resolveLocaleData = function(localeData) {
      console.log("resolve locale data");
      if (localeData.key === undefined || localeData.key === 'ZZ') {
        return localeData;
      }

      /// if 'fmt' doesn't exist, then get it from ZZ
      if (localeData.fmt === undefined) {
        localeData.fmt = dict_addr_formats['ZZ'].fmt;
      }

      if (localeData.lfmt === undefined) {
        localeData.lfmt = localeData.fmt;
      }

      var defaultLocale = dict_addr_formats['ZZ'];

      if (localeData.fmt.search("%Z") !== -1 && 
        localeData["zip_name_type"] === undefined) {
        localeData["zip_name_type"] = defaultLocale.zip_name_type;
      }

      if (localeData.fmt.search("%S") !== -1 && 
        localeData["state_name_type"] === undefined) {
        localeData["state_name_type"] = defaultLocale.state_name_type;
      }

      if (localeData.fmt.search("%C") !== -1 && 
        localeData["locality_name_type"] === undefined) {
        localeData["locality_name_type"] = defaultLocale.locality_name_type;
      }

      if (localeData.fmt.search("%D") !== -1 && 
        localeData["sublocality_name_type"] === undefined) {
        localeData["sublocality_name_type"] = defaultLocale.sublocality_name_type;
      }

      if (localeData.upper === undefined) {
        localeData.upper = defaultLocale.upper;
      }

      if (localeData.require === undefined) {
        localeData.require = defaultLocale.require;
      }

      ///

      let hasStateKeys = localeData.hasOwnProperty("sub_keys");

      if (hasStateKeys) {
        localeData["state_keys"] = localeData.sub_keys.split('~');

        let hasStateNamesInLatin = localeData.hasOwnProperty("sub_lnames");
        let hasStateNames = localeData.hasOwnProperty("sub_names");

        localeData["state_names"] = hasStateNames ? 
          localeData.sub_names.split('~') : localeData.sub_keys.split('~');

        localeData["state_names_latin"] = hasStateNamesInLatin ? 
          localeData.sub_lnames.split('~') : localeData["state_names"];

        console.log("localeData:", localeData);
      }  
      return localeData;
    };

    ///

    Promise.all(arr_promises).then(values => {

      values.forEach((element, index, array) => {
        /// resolve each locale with ZZ if necessary then
        /// append to results
        if (element.key === undefined) {
          element.key = 'ZZ';
        }
        dict_addr_formats[element.key] = resolveLocaleData(element);
      });

      onReadyFunction(dict_addr_formats);
    });
  }
};

