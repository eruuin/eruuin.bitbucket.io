
/*
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/all
*/

$(document).ready(main);

function main() {
  (function showLastModified() {
    let docDate = new Date(document.lastModified);
    let options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

    $("#lastModified").text(docDate.toLocaleDateString('en-US', options));

    options = { hour: '2-digit', minute: '2-digit', timeZoneName: 'short' };
    $("#lastModifiedTime").text(docDate.toLocaleTimeString('en-US', options));
  })();

  let arr_locales  = [ 
    "ZZ", 
    "US", 
    "AU", "CA", "NZ", "ZA", "GB", 
    "--",
    "AM", "AC", "AT",
    "BB", "BR",
    "CN", "CI",
    "EG", "FI", "FR",
    "DE",
    "HK",
    "IN", "ID", "IL", "IT",
    "JP",
    "MY", "MX",
    "NR", "NL",
    "PL",
    "RU",
    "KR",
    "ES",
    "SE",
    "CH",
    "TW", "TH", "TR", 
    "AE",
    "VN"
  ];

  var onReadyCallback = function(dict_of_addr_formats) {
    console.log("dictionary:", dict_of_addr_formats);
    console.log("array of locales:", arr_locales);

    var createCountrySelector = function(arrCountryCodes) {
      var toTitleCase = function(strItem) {
        /// This function was created just for title casing the country names
        let words = strItem.toLowerCase().split(' ');
        let a = words.map(function(word) {
          if (word.startsWith('d\'')) {
            return 'd\'' + word.charAt(2).toUpperCase() + word.slice(3);
          } else {
            return word.charAt(0).toUpperCase() + word.slice(1);
          }
        });
        return a.join(' ');
      };

      /// Create Menu
      /// Start with "1" since we don't want to display "0" (ZZ)

      for (let i = 1; i < arrCountryCodes.length; i++) {
        let country_code = arrCountryCodes[i];
        const menu_separator = '-'.repeat(12);
        let str_item = '<option value="" disabled>' + menu_separator + '</option>';
        if (country_code !== '--') {
          str_item = '<option value="{VALUE}">{DISPLAY}</option>';

          /// Title case the country name
          console.log("country_code:", country_code);
          let country_name = toTitleCase(dict_of_addr_formats[country_code].name);
          str_item = str_item.replace("{VALUE}", country_code).replace("{DISPLAY}", country_name);
        }
        $("#select-country-names").append(str_item);
      } // end-for
    } (arr_locales); /// IIFE

    var createInputForm = function(componentId) {
      return {
        "render": function(html_data) {    
           $(componentId).html(html_data);
        }
      };
    };

    var createOutputForm = function(componentId) {
      return {
        "render": function(html_data) {
          $(componentId).html(html_data);
        }
      };
    };

    var isLangRTL = function(lang) {
        let rtl_langs = ['ar', 'he'];
        return rtl_langs.indexOf(lang) !== -1;
    };

    var inputFormIntl = createInputForm('#address-input-form-intl');
    var outputFormIntl = createOutputForm('#address-output-form-intl');
    var inputFormLocal = createInputForm('#address-input-form-local');
    var outputFormLocal = createOutputForm('#address-output-form-local');
    var renderForms = function(localeId) {
      inputFormIntl.render( util.toInputForm(dict_of_addr_formats[localeId], true));
      outputFormIntl.render( util.toOutputForm(dict_of_addr_formats[localeId], true));

      let textDirection = isLangRTL(dict_of_addr_formats[localeId].lang) ? 'rtl' : 'ltr';
      $('#local-forms').attr('dir', textDirection);

      console.log("****", dict_of_addr_formats[localeId]);
      inputFormLocal.render( util.toInputForm(dict_of_addr_formats[localeId], false));
      outputFormLocal.render( util.toOutputForm(dict_of_addr_formats[localeId], false));
    };

    renderForms('US');

    $("#select-country-names").on('change', function() {
      let countryCode = $('#select-country-names').val();
      console.log("new-value:", countryCode);
      renderForms(countryCode);
    });
  };

  /// -----

  model.init(arr_locales, onReadyCallback);
}

