/// app.js

$(document).ready(main);

function main() {
  $.getJSON(
    "https://desolate-anchorage-31591.herokuapp.com/apps/hanshan/en-US/data/",
    function(results) {
      controller.init(results);
    }
  );
}

var controller = {
  init: function(strings) {
    console.log("stringResources: ", strings);
    
    var poemFactory = createPoemFactory(strings);

    view.display(poemFactory.create());
    timer.start(poemFactory.create);

    $("#output").on("click", function() {
      view.display(poemFactory.create());
      timer.reset(poemFactory.create);
    });
  }
};

function createPoemFactory(stringResources) {
  var getRandomNumber = function(maxValue) {
    return Math.floor(Math.random() * maxValue);
  };
  var getPatternNumber = function() {
    return getRandomNumber(3);
  };
  var getShortPhrase = function() {
    var index = getRandomNumber(stringResources.shortPhrases.length);
    return stringResources.shortPhrases[index];
  };
  var getSingleWord = function() {
    var index = getRandomNumber(stringResources.singleWords.length);
    return stringResources.singleWords[index];
  };
  
  const TabChar = "&nbsp;";
  const Ellipses = "...";

  var poemPatterns = [
    function() {
      return [
        getSingleWord() + Ellipses + getSingleWord(),
        TabChar.repeat(5) + getSingleWord(),
        TabChar.repeat(8) + getShortPhrase()
      ];
    },
    function() {
      return [
        getShortPhrase(),
        TabChar.repeat(3) + getShortPhrase() + Ellipses,
        TabChar.repeat(3) + getShortPhrase()
      ];
    },
    function() {
      return [
        TabChar.repeat(3) + getSingleWord(),
        getShortPhrase(),
        TabChar.repeat(3) + getSingleWord() + ", " + getShortPhrase()
      ];
    }
  ];
  return {
    create: function() {
      var index = getPatternNumber();
      var poem = poemPatterns[index]();
      console.log(poem);
      return poem.join("<br>");
    }
  };
}

var timer = {
  intervalId: undefined,
  start: function(cbFunc) {
    this.intervalId = setInterval(function() {
      $("#output").html(cbFunc);
    }, 5000);
  },
  reset: function(cbFunc) {
    clearInterval(this.intervalId);
    this.start(cbFunc);
  }
};

var view = {
  display: function(poem) {
    $("#output").html(poem);
  }
};

