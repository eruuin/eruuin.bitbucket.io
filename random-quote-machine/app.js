///
/// Reference:
///     https://andruxnet-random-famous-quotes.p.mashape.com/
///

$(document).ready(main);

function main() {
  
  let view = createView();

  var callback = function() {
    /// console.log("callback:", arguments);
    /// console.log("args", arguments[0]);
    view.displayQuote(arguments[0]);
  };

  model.getQuote(callback);

  /* 
   * if displayPanel is clicked,
   * then get a new quote 
   */

  $("#random-quote").click(function() {
    model.getQuote(callback);
  });
}

////

var model = {
  cacheOfQuotes: [],

  getQuote: function(cbFunction) {
    var buildRequest = function() {
      var category = "famous";
      var count = "10";

      var tail = "3FlLNxMM0gtlnsjRfcvB1p1htr0srwsQE2gHghsmhr33cqLpW1"
        .split("")
        .reverse()
        .join("");

      var request =
        "https://andruxnet-random-famous-quotes.p.mashape.com/?" +
        "cat={category}&count={count}" +
        "&mashape-key=" +
        tail;

      return request.replace("{category}", category).replace("{count}", count);
    };

    /// Use the cache.
    /// If cache is empty, fill it with quotes.
    if (this.cacheOfQuotes.length > 0) {
      let content = this.cacheOfQuotes.pop();
      /// view.displayQuote(content);
      cbFunction(content);
    } else {
      let firstPromise = new Promise((resolve, reject) => {
        var request = buildRequest();
        $.getJSON(request, function(result) {
          resolve(result);
        });
      });

      firstPromise.then(results => {
        this.cacheOfQuotes = results.slice();
        this.getQuote(cbFunction);
      });
    }
  }
};


