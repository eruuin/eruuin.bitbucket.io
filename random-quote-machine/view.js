/// view.js

function createView() {
  var buildHrefForTweet = function(quote, author) {
    const tweetBase = "https://twitter.com/intent/tweet?text=";
    const tweetMaxChars = 140;

    let tweet = quote + " -- " + author;

    if (tweet.length > tweetMaxChars) {
      tweet = "length of tweet was more than 140 characters.";
    }

    let encodedURLComp = encodeURIComponent(tweet);
    let value = tweetBase + encodedURLComp;

    console.log("link:", value);

    return value;
  };

  return {
    displayQuote: function(content) {
      const str_href = buildHrefForTweet(content.quote, content.author);

      $("#content").text(content.quote);
      $("#author").text(content.author);
      $("#tweet").attr("href", str_href);
    }
  };
}
