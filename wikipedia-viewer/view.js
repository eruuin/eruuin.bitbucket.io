/// view.js

const pageLink =
  '<div class = "card mb-3">' +
  '<div class = "card-block">' +
  '<div class = "card-title">' +
  '<a href="https://en.wikipedia.org/?curid={pageid}">{title}</a>' +
  "</div>" +
  '<div class = "card-text">' +
  "{extract}" +
  "</div>" +
  "</div>" +
  "</div>";

var view = {
  "renderPageInfo": function(title, pageid, extract) {
    let str_html = pageLink
      .replace("{title}", title)
      .replace("{pageid}", pageid)
      .replace("{extract}", extract);

    $("#results").append(str_html);
  },

  "getSearchText": function() {
    return $("#searchinput").val();
  },

  "clearSearchField": function() {
    $("#searchinput").val("");
    $("#results").empty();
  },

  "noPagesFound": function() {
    $("#message").append("0 Pages found.");
  },

  "pagesFound": function() {
    $("#message").append("Pages found.");
  },

  "searchingFor": function(text) {
   $("#message").text('Searching for "' + text + '" ... ');
  },

  "hideKeyboard": function() {
    $("input").blur();
  }
};

