///
/// 
///

$(document).ready(main);

function main() {
  controller.init();
}

var controller = {
  "init": function() {
    var eventHandler = function(event) {
      const EnterKey = 13;

      if (event.which == EnterKey) {
        console.log("Enter Key is Down");

        /// read the input control and then call the search function
        let searchText = view.getSearchText();

        view.searchingFor(searchText);
        view.clearSearchField();
        view.hideKeyboard();

        model.search(searchText, view);
      }
    };

    /// detect Enter Key inside the Input field
    $("#searchinput").keydown(eventHandler);
  }
};
