///
/// model.js
///

/*
----
Example: https://en.wikipedia.org/?curid={pageid}
----
Example: https://en.wikipedia.org/?curid=3090411
----
*/

var model = {
  search: function(searchText, callback_obj) {
  var buildReqData = function(searchText) {
    let uriComponent = encodeURIComponent(searchText);
    const reqTemplate =
      "action=query" +
      "&format=json&generator=search&gsrnamespace=0&gsrlimit=10&gsrsearch={text}" +
      "&prop=pageimages|extracts" +
      "&pilimit=max&exintro&explaintext&exsentences=1&exlimit=max" +
      "&callback=?";

    return reqTemplate.replace("{text}", uriComponent);
  };

  const endPoint = "https://en.wikipedia.org/w/api.php";

  let reqData = buildReqData(searchText);

  console.log(reqData);

  let jqXmlHttpReq = $.getJSON(
    endPoint, reqData,
    (result, status, xmlHttpReq) => {
      console.log("Result:", result);
      console.log("Status:", status);
      console.log("XmlHttpReq:", xmlHttpReq);

      if (result.query === undefined) {
        callback_obj.noPagesFound();
        return;
      }

      let keys = Object.keys(result.query.pages);
      /// console.log(keys);

      keys.forEach((element, index, array) => {
        let entry = result.query.pages[element];

        callback_obj.renderPageInfo(entry.title, entry.pageid, entry.extract);

        console.log(entry);
      });
      callback_obj.pagesFound();
    }
  )
  /// this isn't doing anything in particular but good to know these are here.
  .done(() => console.log("search: done();"))
  .fail(() => console.log("search: fail();"))
  .always(() => console.log("search: always();"));

  /// this isn't doing anything in particular but good to know these are here.
  jqXmlHttpReq
    .done(() => {
      console.log("jqXmlHttpReq: done");
    })
    .fail(() => {
      console.log("jqXmlHttpReq: fail");
    })
    .always(() => {
      console.log("jqXmlHttpReq: always");
    });

  } /// end-function
}; /// end-model

