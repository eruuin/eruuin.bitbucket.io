/// app.js

var utils = {
  getLocation: function() {
    /// Call ipinfo.io to get location
    var promise = new Promise((resolve, reject) => {
      if (navigator.geolocation) {
        var getPosition = function(position) {
          let location =
            "" + position.coords.latitude + "," + position.coords.longitude;
          console.log("location:", location);
          let ipinfo = { loc: location };
          let results = { ipinfo: ipinfo };

          resolve(results);
        };

        var handleError = function(error) {
          let reason; // undefined

          switch (error.code) {
            case error.PERMISSION_DENIED:
              reason = "User denied the request for Geolocation.";
              break;
            case error.POSITION_UNAVAILABLE:
              reason = "Location information is unavailable.";
              break;
            case error.TIMEOUT:
              reason = "The request to get user location timed out.";
              break;
            case error.UNKNOWN_ERROR:
              reason = "An unknown error occurred.";
              break;
            default:
              reason = "Reason: it should never get here.";
              break;
          }
          console.log("Reason:", reason);
          $("#debug-message").text(reason);

          $.getJSON("https://ipinfo.io/json", function(ipinfo) {
            console.log("get location****", ipinfo);
            let results = { ipinfo: ipinfo };
            resolve(results);
          });
        };
        navigator.geolocation.getCurrentPosition(getPosition, handleError);
      } // end-if
    });
    return promise;
  },

  SecondsToDateString: function(secs) {
    const options = { hour: "2-digit", minute: "2-digit" };
    return new Date(secs * 1000)
      .toLocaleString(undefined, options)
      .toLowerCase();
  },

  KelvinToFahrenheit: function(k) {
    return Math.floor(Math.round(1.8 * (k - 273) + 32));
  }
};

var getKey = function() {
  const apiKey = "482294b5b1c61ffee27246fc3e84ef8a";
  return apiKey.split("").reverse().join("");
};

var buildCall = function(callType, location) {
  const crossOrigin = "https://cors-anywhere.herokuapp.com/";
  let apiCall =
    crossOrigin +
    "http://api.openweathermap.org/data/2.5/{type}?lat={lat}&lon={lon}&APPID={APIKEY}";
  let coords = location.split(",");

  return apiCall
    .replace("{type}", callType)
    .replace("{lat}", coords[0])
    .replace("{lon}", coords[1])
    .replace("{APIKEY}", getKey());
};

function captureTemperature(kelvinTemp) {
  var convertToFahr = function(k) {
    let fahr = 1.8 * (k - 273.15) + 32;
    // return fahr.toFixed(2);
    return Math.round(fahr.toFixed(2));
  };

  var convertToCelcius = function(k) {
    let celcius = k - 273.15;
    // return celcius.toFixed(2);
    return Math.round(celcius.toFixed(2));
  };

  var fahrTemperature = convertToFahr(kelvinTemp);
  var celciusTemperature = convertToCelcius(kelvinTemp);

  return {
    getFahr: function() {
      return fahrTemperature;
    },
    getCelcius: function() {
      return celciusTemperature;
    }
  };
}

$(document).ready(function() {
  ///
  /// Reference:
  ///     https://html5hive.org/how-to-chain-javascript-promises/

  utils
    .getLocation()
    .then(model.getCurrentTemp)
    .then(model.getForecast)
    .then(view.render)
    .catch(function(err) {
      /// TODO
      console.log("error detected:", err);
    });

  $("#displayPanel").click(function() {
    view.toggle();
  });
});

