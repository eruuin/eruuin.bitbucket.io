/// view.js

var view = {
  temp_low: 0,
  temp_high: 0,
  temp_current: 0,

  render: function(results) {
    var isDaytime = function() {

      /*
       * To determine if it is daytime,
       * check if the current time in seconds after midnight is
       * in the range of the sunrise and sunset where sunrise and sunset
       * are expressed in seconds after midnight.
       */

      let now = new Date();
      let sunrise = new Date(results.weather.sys.sunrise * 1000);
      let sunset = new Date(results.weather.sys.sunset * 1000);

      console.log("sunrise:", sunrise.getHours(), sunrise.getMinutes() );
      console.log("sunset:", sunset.getHours(), sunset.getMinutes() );
      console.log("now:", now.getHours(), now.getMinutes());

      let nowSeconds = now.getHours() * 60 + now.getMinutes();
      let sunriseSeconds = sunrise.getHours() * 60 + sunrise.getMinutes();
      let sunsetSeconds = sunset.getHours() * 60 + sunset.getMinutes();

      return (
        sunriseSeconds <= nowSeconds && nowSeconds <= sunsetSeconds
      );
    };
    var promise = new Promise((resolve, reject) => {
      console.log("render data:", results);

      view.temp_current = captureTemperature(results.weather.main.temp);
      view.temp_low = captureTemperature(results.weather.main.temp_min);
      view.temp_high = captureTemperature(results.weather.main.temp_max);

      view.renderTemps();

      const imgPath = "https://openweathermap.org/img/w/{icon-name}.png";
      let imgSrc = imgPath.replace(
        "{icon-name}",
        results.weather.weather[0].icon
      );
      $("#weather-icon").attr("src", imgSrc);
      $("#city").text(results.weather.name);
      $("#location").text(results.ipinfo.loc);
      $("#weather-icon").attr("title", results.weather.weather[0].main);

      console.log(
        "Sunrise:",
        utils.SecondsToDateString(results.weather.sys.sunrise)
      );
      console.log(
        "Sunset:",
        utils.SecondsToDateString(results.weather.sys.sunset)
      );
      console.log("Humidity: %d%", results.weather.main.humidity);

      $("#humidity").text("" + results.weather.main.humidity + "%");

      let s = new Date().toLocaleTimeString(undefined, {
        hour: "2-digit",
        minute: "2-digit"
      });
      $("#local-time").text(s);

      if (isDaytime()) {
        console.log("day time");
        $("body").css("background-image", 
          "url('https://res.cloudinary.com/homputer/image/upload/v1500588083/paragliding-2378387_1280_x7zt7x.jpg')"
        );
        $("#sunrise").text(
          utils.SecondsToDateString(results.weather.sys.sunrise)
        );
        $("#sunset").text(
          utils.SecondsToDateString(results.weather.sys.sunset)
        );
      } else {
        console.log("night time");
        $("body").css("background-image", 
          "url('https://res.cloudinary.com/homputer/image/upload/v1500610326/star-2211589_1920_n2grzv.jpg')"
        );
        $("#day-or-night").text("dark:");
        $("#sunrise").text(
          utils.SecondsToDateString(results.weather.sys.sunset)
        );
        $("#sunset").text(
          utils.SecondsToDateString(results.weather.sys.sunrise)
        );
      }

      /// Not using forecast data yet.
      /// showForecastData(results.forecast);
    });
    return promise;
  },

  renderTemps: function() {
    let current = 0;
    let low = 0;
    let high = 0;
    let suffix = "F";

    if (view.mode === "F") {
      current = view.temp_current.getFahr();
      low = view.temp_low.getFahr();
      high = view.temp_high.getFahr();
    } else {
      current = view.temp_current.getCelcius();
      low = view.temp_low.getCelcius();
      high = view.temp_high.getCelcius();
      suffix = "C";
    }

    $("#temp-current").html(current + "&#176;");
    $("#temp-unit").html(suffix);
    $("#today-low").html(low + "&#176;");
    $("#today-high").html(high + "&#176;");
    $("#distance-min-cur").text("|".repeat(current - low));
    $("#distance-cur-max").text("|".repeat(high - current));
  },

  mode: "F",

  toggle: function() {
    view.mode = view.mode === "F" ? "C" : "F";
    view.renderTemps();
    console.log("display in ", view.mode);
  }
};

