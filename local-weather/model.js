/// model.js


var getWeather = function(input_obj, resolve) {
  /// console.log("get weather for this location:", ipinfo);

  let apiCall = buildCall("weather", input_obj.ipinfo.loc);

  /// console.log("apiCall:", apiCall);

  $.getJSON(apiCall, function(data) {
    console.log("got the Weather: ", data);

    let timePosted = new Date(data.dt * 1000).toLocaleString();
    console.log("Time Posted:", timePosted);
    input_obj.weather = data;
    resolve(input_obj);
  });
};

var getWeatherForecast = function(input_obj, resolve) {
  let apiCall = buildCall("forecast", input_obj.ipinfo.loc);

  $.getJSON(apiCall, function(data) {
    console.log("got forecast");
    input_obj.forecast = data;
    resolve(input_obj);
  });
};

var showForecastData = function(data) {
  let min_temps = {};
  let max_temps = {};

  for (let i = 0; i < data.cnt; i += 1) {
    var min_temp = utils.KelvinToFahrenheit(data.list[i].main.temp_min);
    if (min_temp in min_temps) {
      min_temps[min_temp] += 1;
    } else {
      min_temps[min_temp] = 1;
    }
  }

  for (let i = 0; i < data.cnt; i += 1) {
    var max_temp = utils.KelvinToFahrenheit(data.list[i].main.temp_max);
    if (max_temp in max_temps) {
      max_temps[max_temp] += 1;
    } else {
      max_temps[max_temp] = 1;
    }
  }

  console.log("min_temps:", min_temps);
  console.log("max_temps:", max_temps);
};

var model = {
  getCurrentTemp: function(input_obj) {
    var promise = new Promise((resolve, reject) => {
      getWeather(input_obj, resolve);
    });
    return promise;
  },
  getForecast: function(input_obj) {
    var promise = new Promise((resolve, reject) => {
      getWeatherForecast(input_obj, resolve);
    });
    return promise;
  }
};

